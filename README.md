ob-gtkgreet-sway
================

This package installs scripts and config to run a Wayland session under Obarun
such that the [gtgreet](https://git.sr.ht/~kennylevinsen/gtkgreet) greeter can
be launched using [sway](https://swaywm.org).

> IMPORTANT:
> This package enables passwordless sudo privilege for the `obgreet` user to execute
> `/sbin/reboot` and `/sbin/poweroff` commands on the local host.
> If this is not acceptable, then these privileges can be revoked by deleting the
> installed `/etc/sudoers.d/10_obgreet` file.

## Dependencies

Requires:
* sway
* wlroots >= 0.13
* [greetd-66serv](https://framagit.org/xprl-obarun-contrib-wayland/greetd-66serv) (which itself depends on [greetd](https://git.obarun.org/pkg/obcommunity/greetd) via obcommunity repo.)
* [seatd-66serv](https://git.obarun.org/pkg/observice/seatd-66serv) via observice repo.

## Usage

Install the ob-gtkgreet-sway package and dependencies.


Create a new service tree to launch the desktop greeter: (as root)
```console
# 66 tree create -o depends=global desktop
# 66 -t desktop enable seatd greetd
# 66 -t desktop enable boot-user@obgreet
```

#### configure boot-user
Configure the `obgreet` user services by doing the boot-user@ dance:
```console
# 66 -t desktop configure -e nano boot-user@obgreet
```
Ensure services for `obgreet` are configured as:
```text
# file: /etc/66/conf/boot-user@obgreet/.../boot-user@obgreet
DISPLAY_MANAGER=!greetd
CONSOLE_TRACKER=!seatd
XDG_RUNTIME=!yes
# disable X11 desktop cmdline:
# DESKTOP_CMDLINE=!jwm
```
Re-apply the modified configuration:
```console
# 66 -t desktop reconfigure boot-user@obgreet
```

#### configure greetd
Now, one more configuration step; this time for greetd:
```console
# 66 -t desktop configure -e nano greetd
```
Change the greetd config to:
```text
cmd_args=!-c /etc/greetd/config.obgreet.toml
```
...and apply those changes:
```console
# 66 -t desktop reconfigure greetd
```

#### launch
Finally, start the desktop tree to launch the greeter:
```console
# 66 tree start desktop
```

Et voilà!

### Greeter Configuration

The behaviour and layout of the greeter display is configured via
`/etc/greetd/sway.config`. However, rather than modifying this file, additional
configuration files can be added under `/etc/greetd/sway.config.d`. Files placed
in this directory will be included in the base configuration.

**Note:** Under the default configuration, a menu bar with 🟨|poweroff|🟨|reboot|🟨
functions can be activated by pressing `Ctrl-Alt-q`.

The style of the `gtkgreet` application itself can be modified by placing an
appropriate Gtk3.0 CSS stylesheet in`/var/lib/obgreet/.config/gtkgreet/gtkgreet.css`.


### User Sessions

All users should be able to log in and launch a 'zsh' session via the greeter.
For users to be able to launch a 'sway-desktop' session, the 'XDG_RUNTIME_DIR' directory
needs to be mounted and running. The way to do this is to repeat the boot-user@ dance for
each user:

1. Enable a `boot-user@` module service for the user: (as root)
   ```console
   # 66 -t desktop enable boot-user@fred
   # 66 -t desktop configure -e nano boot-user@fred
   ```
   (replacing 'fred' with the appropriate username)

   This time, in the boot-user config, disable all services *except* XDG_RUNTIME:
   ```text
   # file: /etc/66/conf/boot-user@fred/.../boot-user@fred
   XDG_RUNTIME=!yes
   # disable display manager, console tracker and X11 desktop cmdline:
   # DESKTOP_CMDLINE=!jwm
   # DISPLAY_MANAGER=!greetd
   # CONSOLE_TRACKER=!seatd
   ```

   Then re-enable the service to apply the modified config:
   ```console
   # 66 -t desktop reconfigure boot-user@fred
   ```

   The `boot-user@fred` module with XDG_RUNTIME enables the `mount-run@fred` service.
   It is this service that must be up before the sway-desktop session can be started.
   
2. Secondly, create a `.wayland-session` environment config file. This can either be 
   in XDG_CONFIG_DIR (typically ~/.config), or can be in the user's home directory.
   An example config is installed at `/etc/66/proto.user-wayland-session`.
   Therefore:
   ```console
   > cp /etc/66/proto.user-wayland-session  ~fred/.wayland-session
   > chown fred:fred  ~fred/.wayland-session
   ```
   Edit this file as appropriate.

   The .wayland-session config file unsets the user's `DISPLAY` environment variable;
   **this is necessary to allow sway to launch successfully**.

### Logging

By default, the greeter session writes logs to `/var/lib/obgreet/.66/logs/wayland-session/`

User session logs are written to `${HOME}/.66/logs/wayland-session`

The logging directory location can be modified (or logging can be disabled) by changing
or removing the USERSESSIONLOGDIR environment variable. This is normally set in the
user's `.wayland-session` config file.

### Advanced Configuration

If a user's`.wayland-session`configuration exports a value for `USERSESSIONSERVICETREE`,
then the corresponding 66-tree will be brought up at the start of the user's desktop
session, and brought down when the user ends the desktop session.

By default, every user has a tree named `global`.  
By default, `USERSESSIONSERVICETREE` is set to `global`.

Therefore, if user fred wants to automatically start a `dbus` session bus for each desktop session,
they could achieve this via: (as fred)

``` shellsession
> 66 -t global enable dbus-session@fred
```

To start a different tree, or to disable this automatic starting of the `global` tree, edit the
user's .wayland-session file.
