# Obarun        : 66 init/supervisor
# Maintainer    : Gavin Falconer <gavin [at] expressivelogic [dot] net>
# PkgSource     : https://framagit.org/xprl-obarun-contrib-wayland/gtkgreet-sway
#--------------------------------------------------------------------------------------

pkgname=ob-gtkgreet-sway

pkgdesc="gtkgreet for greetd displayed via sway compositor"

pkgver=0.2.0
pkgrel=2

url="https://framagit.org/xprl-obarun-contrib-wayland/${pkgname}.git"

track=branch
target=master
source=("${pkgname}::git+${url}#${track}=${target}")

#--------------------------------------------------------------------------------------

prepare() {
  cd "${srcdir}/${pkgname}"

  # inject host name into sudoers file
  HOST=$(uname -n)
  sed "s:@HOST@:${HOST}:" -i "obgreet.sudoers"
}

#--------------------------------------------------------------------------------------

package() {
  cd "${srcdir}/${pkgname}"

  # binaries installed in /usr/share/greetd and linked from /usr/bin
  install -Dm0755 -t "$pkgdir/usr/share/greetd" \
    "wayland-session" "sway-desktop"

  install -d "$pkgdir/usr/bin"
  ln -s -t "$pkgdir/usr/bin" "/usr/share/greetd/wayland-session"
  ln -s -t "$pkgdir/usr/bin" "/usr/share/greetd/sway-desktop"

  # /etc/66
  install -Dm0644 "66.wayland-session" \
    "$pkgdir/etc/66/.wayland-session"
  install -Dm0644 "proto.user-wayland-session" \
    "$pkgdir/etc/66/proto.user-wayland-session"

  # /etc/greetd files
  install -Dm0644 "greetd/config.obgreet.toml" \
    "$pkgdir/etc/greetd/config.obgreet.toml"

  install -d "$pkgdir/etc/greetd/sway.config.d"
  install -Dm0644 -t "$pkgdir/etc/greetd" \
    "obgreet/sway.config" "obgreet/environments"

  # sysysers
  install -Dm0644 "obgreet.sysusers" \
    "$pkgdir/usr/lib/sysusers.d/obgreet.conf"

  # sudoers
  install -dm0750 "$pkgdir/etc/sudoers.d"
  install -Dm0640 "obgreet.sudoers" \
    "$pkgdir/etc/sudoers.d/10_obgreet"
}

#--------------------------------------------------------------------------------------

install='obgreet.install'

arch=('x86_64')

depends=(
  'greetd'
  'greetd-gtkgreet'
  'obsysusers'
  's6>=0.7'
  'execline'
  '66-tools'
  'sway'
  'wlroots>=0.13'
)
makedepends=('git')
optdepends=()

backup=(
  'etc/greetd/config.obgreet.toml'
  'etc/greetd/environments'
  'etc/greetd/sway.config'
)

#--------------------------------------------------------------------------------------

sha512sums=('SKIP')

license=('ISC')
